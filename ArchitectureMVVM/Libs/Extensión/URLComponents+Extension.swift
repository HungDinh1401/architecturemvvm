//
//  URLComponents+Extension.swift
//  ArchitectureMVVM
//
//  Created by Hưng' Mac mini on 12/1/21.
//

import Foundation

extension URLComponents {
    func addingApiKey(_ apiKey: String) -> URLComponents {
        var copy = self
        copy.queryItems = [URLQueryItem(name: "api_key", value: apiKey)]
        return copy
    }
    
    var request: URLRequest? {
        url.map { URLRequest.init(url: $0) }
    }
}
