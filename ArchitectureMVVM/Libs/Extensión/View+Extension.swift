//
//  View+Extension.swift
//  ArchitectureMVVM
//
//  Created by Hưng' Mac mini on 12/1/21.
//

import Foundation
import SwiftUI

extension View {
    func eraseToAnyView() -> AnyView { AnyView(self) }
}
