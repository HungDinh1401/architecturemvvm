//
//  ArchitectureMVVMApp.swift
//  ArchitectureMVVM
//
//  Created by Hưng' Mac mini on 11/29/21.
//

import SwiftUI

@main
struct ArchitectureMVVMApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
