//
//  ContentView.swift
//  ArchitectureMVVM
//
//  Created by Hưng' Mac mini on 11/29/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        return MovieListView(viewModel: MoviesListViewModel())
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
