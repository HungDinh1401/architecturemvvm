//
//  Movie.swift
//  ArchitectureMVVM
//
//  Created by Hưng' Mac mini on 12/1/21.
//

import Foundation

struct MovieDTO: Codable {
    let id: Int
    let title: String
    let poster_path: String?
    
    var poster: URL? { poster_path.map { MoviesAPI.imageBase.appendingPathComponent($0) } }
}
