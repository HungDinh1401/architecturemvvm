//
//  Page.swift
//  ArchitectureMVVM
//
//  Created by Hưng' Mac mini on 12/1/21.
//

import Foundation

struct PageDTO<T: Codable>: Codable {
    let page: Int?
    let total_results: Int?
    let total_pages: Int?
    let results: [T]
}
